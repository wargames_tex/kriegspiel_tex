#
#
#
NAME		:= KriegSpiel
VERSION		:= 1.0
LATEX		:= lualatex
LATEX_FLAGS	:= -interaction=nonstopmode	\
		   -file-line-error		\
		   -synctex=1			\
		   -shell-escape
REDIR		:= > /dev/null 2>&1 
DOCFILES	:= $(NAME).tex		\
		    hexes.tex		\
		    tables.tex		\
		    counters.tex	\
		    front.tex		\
		    mat.tex		\
		    materials.tex	\
		    oob.tex		\
		    optional.tex	\
		    preface.tex		\
		    rules.tex		\
		    scenario.tex	\
		    solitaire.tex  	
STYFILES	:= ks.sty		\
		   commonwg.sty
SIGNATURE	:= 32
PAPER		:= --a4paper
BOOKLETS	:= $(NAME)Booklet.pdf
TARGETS		:= $(NAME).pdf 		\
		   materials.pdf 	


TARGETSA4	:= $(TARGETS:%.pdf=%A4.pdf) 			\
		   $(BOOKLETS:%Booklet.pdf=%A4Booklet.pdf)	
TARGETSUS	:= $(TARGETS:%.pdf=%US.pdf) 			\
		   $(BOOKLETS:%Booklet.pdf=%USBooklet.pdf)	

ifdef VERBOSE	
MUTE		:=
REDIR		:=
LATEX_FLAGS	:= -synctex=1	-shell-escape
else
MUTE		:= @
REDIR		:= > /dev/null 2>&1 
endif

%.aux:	%.tex
	@echo "LATEX(1)	$*"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $* $(REDIR)

%.pdf:	%.aux
	@echo "LATEX(2)	$*"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $* $(REDIR)

%A4.aux:	%.tex
	@echo "LATEX(1)	$*A4"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A4 $* $(REDIR)

%A4.pdf:	%A4.aux
	@echo "LATEX(2)	$*A4"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A4 $* $(REDIR)

%US.aux:	%.tex
	@echo "LATEX(1)	$*US"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*US $* $(REDIR)

%US.pdf:	%US.aux
	@echo "LATEX(2)	$*US"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*US $* $(REDIR)

%Booklet.pdf:%.pdf
	@echo "BOOKLET	$*"
	$(MUTE)pdfjam 				\
		--landscape 			\
		--suffix book 			\
		--signature '$(SIGNATURE)' 	\
		$(PAPER) 			\
		--no-tidy 			\
		--outfile $@ 			\
		-- $< '1-$(SIGNATURE)' $(REDIR)

all:	$(TARGETS)
a4:	$(TARGETSA4)
us:	$(TARGETSUS)

labels.tex:$(NAME).aux
	@echo "LABELS $*"
	$(MUTE)grep "newlabel{opt:" $< > $@

labels%.tex:$(NAME)%.aux
	@echo "LABELS $*"
	$(MUTE)grep "newlabel{opt:" $< > $@


cache:
	$(MUTE)mkdir -p cache
	$(MUTE)if test ! -f cache/.gitignore ; then touch cache/.gitignore; fi

distdir:$(TARGETS) README.md
	@echo "DISTDIR	$(NAME)-$(VERSION)"
	$(MUTE)mkdir -p $(NAME)-$(VERSION)
	$(MUTE)cp $^ $(NAME)-$(VERSION)/
	$(MUTE)touch $(NAME)-$(VERSION)/*

dist:	distdir
	@echo "DIST	$(NAME)-$(VERSION).zip"
	$(MUTE)zip -r $(NAME)-$(VERSION).zip $(NAME)-$(VERSION)
	$(MUTE)rm -rf $(NAME)-$(VERSION)

distdirA4:$(TARGETSA4) README.md
	@echo "DISTDIR	$(NAME)-A4-$(VERSION)"
	$(MUTE)mkdir -p $(NAME)-A4-$(VERSION)
	$(MUTE)cp $^ $(NAME)-A4-$(VERSION)/
	$(MUTE)touch $(NAME)-A4-$(VERSION)/*

distA4:	distdirA4
	@echo "DIST	$(NAME)-A4-$(VERSION).zip"
	$(MUTE)zip -r $(NAME)-A4-$(VERSION).zip $(NAME)-A4-$(VERSION)
	$(MUTE)rm -rf $(NAME)-A4-$(VERSION)

distdirUS:$(TARGETSUS) README.md
	@echo "DISTDIR	$(NAME)-US-$(VERSION)"
	$(MUTE)mkdir -p $(NAME)-US-$(VERSION)
	$(MUTE)cp $^ $(NAME)-US-$(VERSION)/
	$(MUTE)touch $(NAME)-US-$(VERSION)/*

distUS:	distdirUS
	@echo "DIST	$(NAME)-US-$(VERSION).zip"
	$(MUTE)zip -r $(NAME)-US-$(VERSION).zip $(NAME)-US-$(VERSION)
	$(MUTE)rm -rf $(NAME)-US-$(VERSION)

clean:
	@echo "CLEAN"
	$(MUTE)rm -f *~ *.log *.aux *.out *.lot *.lof *.toc *.auxlock
	$(MUTE)rm -f *.synctex* *.pdf _region_.tex
	$(MUTE)rm -f $(TARGETS) $(TARGETSA4) $(TARGETSUS) 

realclean: clean
	@echo "CLEAN ALL"
	$(MOTE)rm -rf $(NAME)-$(VERSION)
	$(MOTE)rm -rf $(NAME)-US-$(VERSION)
	$(MOTE)rm -rf $(NAME)-A4-$(VERSION)
	$(MUTE)rm -rf cache/*

distclean:realclean
	@echo "CLEAN DIST"
	$(MUTE)rm -rf *.zip

show:
	@echo "BOOKLETS"
	@$(foreach t, $(BOOKLETS), echo "  $(t)";)
	@echo "TARGETS:"
	@$(foreach t, $(TARGETS), echo "  $(t)";)
	@echo "US TARGETS:"
	@$(foreach t, $(TARGETSUS), echo "  $(t)";)
	@echo "A4 TARGETS:"
	@$(foreach t, $(TARGETSA4), echo "  $(t)";)

$(NAME).aux:	$(DOCFILES)	$(DOCFILES) 	$(STYFILES)
materials.aux:	materials.tex	labels.tex      $(STYFILES)

$(NAME)A4.aux:	$(DOCFILES)	$(STYFILES)
materialsA4.aux:materials.tex	labelsA4.tex    $(STYFILES)

$(NAME)US.aux:	$(DOCFILES)	$(STYFILES)
materialsUS.aux:materials.tex	labelsUS.tex    $(STYFILES)


$(NAME)USBooklet.pdf:	PAPER=--letterpaper
$(NAME)USBooklet.pdf:	$(NAME)US.pdf
$(NAME)A4Booklet.pdf:	$(NAME)A4.pdf

docker-prep:
	apt update
	apt install -y wget
	wget "https://gitlab.com/wargames_tex/wargame_tex/-/jobs/artifacts/master/download?job=dist" -O wargame.zip
	unzip -o wargame.zip -d ${HOME}
	mv ${HOME}/public ${HOME}/texmf
	make clean

docker-artifacts: distdirA4 distdirUS

#
# EOF
#

