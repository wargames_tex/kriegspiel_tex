# Kriegspiel 

[[_TOC_]]

This is my rework of the wargame _Kriegspiel_. 

## The files 

The distribution consists of the following files 

- [KriegSpielA4.pdf][] This is a single document that contains
  everything: The rules, charts, counters, and boards.
  
  This document is meant to be printed on a duplex A4 printer.  The
  charts, OOB charts, counters, and board will be on separate sheets
  and can be separated from the rules.  The rules will be 20 pages in
  this case.
  
- [KriegSpielA4Booklet.pdf][] This document _only_ contains the
  rules.  It is meant to be printed on a duplex A4 printer with long
  edge binding.  This will produce 8 sheets which should be folded
  down the middle of the long edge, and stabled to form an A5 booklet
  of the rules.
  
- [materialsA4.pdf][] This document contains the charts, counters,
  boards, and mat on separate pages.   Print and glue to sturdy
  cardboard and cut out counters using a sharp knife.   Note the
  unit counters are double sided.  Therefore, cut out the front side
  block and glue to cardboard.  The cut out the back side block and
  glue to the back of the cardboard, taking care to align with the
  front, and then cut out the counters.   I prefer to glue on to 1.5mm
  cardboard, then do one cut (not all the way through) along row and
  column borders on both side.  After that, I can typically separate
  out the individual counters by hand, possibly using the knife a to
  help a little. 

If you only have access to US Letter printer, you can use the
following files

| *A4 Series*                 | *Letter Series*             |
| ----------------------------|-----------------------------|
| [KriegSpielA4.pdf][]        | [KriegSpielUS.pdf][]  	    |
| [KriegSpielA4Booklet.pdf][] | [KriegSpielUSBooklet.pdf][] |
| [materialsA4.pdf][]		  | [materialsUS.pdf][]		    |

Note, the US letter files are scaled down to 95% relative to the A4
files.  This is to fit everything in on the same scale.  This should
not be a problem for most users as most of the world is using A4
rather than the somewhat obscure Letter format. 

Download [artifacts.zip][] for all files.

## Previews 

![](.imgs/front.png)
![](.imgs/boarda.png)
![](.imgs/boardb.png)
![](.imgs/counters.png)
![](.imgs/markers.png)
![](.imgs/mat.png)
![](.imgs/oob.png)
![](.imgs/markers-overview.png)
![](.imgs/charts.png)
![](.imgs/other-charts.png)
![](.imgs/optionals.png)

## Implementation 

The whole package is implemented in LaTeX using my package
[_wargame_](https://gitlab.com/wargames_tex/wargame_tex).  This package,
combined with the power of LaTeX, produces high-quality, documents,
with vector graphics to ensure that every thing scales.  The documents
are prepared for externalised graphics.

[artifacts.zip]: https://gitlab.com/wargames_tex/kriegspiel_tex/-/jobs/artifacts/master/download?job=dist

[KriegSpielA4.pdf]: https://gitlab.com/wargames_tex/kriegspiel_tex/-/jobs/artifacts/master/file/KriegSpiel-A4-master/KriegSpielA4.pdf?job=dist
[KriegSpielA4Booklet.pdf]: https://gitlab.com/wargames_tex/kriegspiel_tex/-/jobs/artifacts/master/file/KriegSpiel-A4-master/KriegSpielA4Booklet.pdf?job=dist
[materialsA4.pdf]: https://gitlab.com/wargames_tex/kriegspiel_tex/-/jobs/artifacts/master/file/KriegSpiel-A4-master/materialsA4.pdf?job=dist

[KriegSpielUS.pdf]: https://gitlab.com/wargames_tex/kriegspiel_tex/-/jobs/artifacts/master/file/KriegSpiel-US-master/KriegSpielUS.pdf?job=dist
[KriegSpielUSBooklet.pdf]: https://gitlab.com/wargames_tex/kriegspiel_tex/-/jobs/artifacts/master/file/KriegSpiel-US-master/KriegSpielUSBooklet.pdf?job=dist
[materialsUS.pdf]: https://gitlab.com/wargames_tex/kriegspiel_tex/-/jobs/artifacts/master/file/KriegSpiel-US-master/materialsUS.pdf?job=dist
