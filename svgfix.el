(query-replace-regexp

;; (\([- 0-9.]+\),\([- 0-9.]+\))
;; (\,(* .02811678428116784279 (string-to-number \1)),\,(* -.02811678428116784279 (string-to-number \2)))
;; (\,(+  (string-to-number \1)),\,(+  (string-to-number \2)))
;; (\,(format "%8.4f" (string-to-number \1)),\,(format "%8.4f" (string-to-number \2)))
;;
;; \.\. controls\s-+\(([- 0-9,.]+)\) and\s-*\(([- 0-9,.]+)\) \.\.\s-+
;; .. controls \1 and \2
    .. 
;;
;; \usetikzlibrary{calc}
;; \makeatletter
;; \def\extractcoord#1#2#3{
;;   \path let \p1=(#3) in \pgfextra{
;;       \pgfmathsetmacro#1{\x{1}/\pgf@xx}
;;       \pgfmathsetmacro#2{\y{1}/\pgf@yy}
;;     \xdef#1{#1}
;;     \xdef#2{#2}
;;   };
;; }
;; \makeatother
;; \newcommand{\showboundingbox}{%
;;   \extractcoord\xa\ya{current bounding box.south west}
;;   \extractcoord\xb\yb{current bounding box.north east}
;;   \node [draw=red,anchor=north east] at (current bounding box.north east)
;;     {(\xa, \ya) (\xb, \yb)};
;;   \messagE{^^JBB:` (\xa,\ya)x(\xb,\yb)'}
;; }
;;;; 
